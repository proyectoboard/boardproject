<?php 
	
	// DEEP LINK
	// php://input
	$post = json_decode(file_get_contents("php://input"), true);

	require('config/conn.php');

	// CLASSES
	require('classes/Tarea.php');

	$conn 			= new Conn();
	$objTarea		= new Tarea();

	$id 		= "";
	if(isset($_GET['id'])){
		$id = $_GET['id'];
	}


	if($ruteo["model"] == "tarea-new"){
		$result = $objTarea->insertarTarea($conn, $post);
		} else if ($ruteo["model"] == "tarea-edit"){
		$result = $objTarea->editarTarea($conn, $post, $id);
		
	}
?>