<?php 
	
	// DEEP LINK
	// php://input
	$post = json_decode(file_get_contents("php://input"), true);

	require('config/conn.php');

	// CLASSES
	require('classes/Usuario.php');

	$conn 			= new Conn();
	$objUsuario		= new Usuario();

	if($ruteo["model"] == "login"){
		$result = $objUsuario->login($conn, $post);
	} else if($ruteo["model"] == "usuario-new"){
		$result = $objUsuario->ingresarUsuario($conn, $post);
	} else if($ruteo["model"] == "usuario-edit"){
		$result = $objUsuario->editarUsuario($conn, $post);
	}
	
?>