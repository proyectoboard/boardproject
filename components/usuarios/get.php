<?php 

	//
	require('config/conn.php');

	// CLASSES
	require('classes/Usuario.php');

	$conn 			= new Conn();
	$objUsuario 	= new Usuario();

	$id 		= "";
	if(isset($_GET['id'])){
		$id = $_GET['id'];
	}
	if($ruteo["model"] == "usuarios"){
		$result = $objUsuario->obtenerUsuarios($conn,$id);

	} else if ($ruteo["model"] == "usuario-by-id"){
		$result = $objUsuario->obtenerUsuarioPorId($conn,$id);

	} else if($ruteo["model"]== "obtenerUsuariosProgramador"){
		$result = $objUsuario->obtenerUsuariosProgramador($conn);
	}
	

	
?>