<?php 

	//
	require('config/conn.php');

	// CLASSES
	require('classes/Comentario.php');

	$conn 				= new Conn();
	$objComentario	 	= new Comentario();

	$id 		= "";
	if(isset($_GET['id'])){
		$id = $_GET['id'];
	}
	
	if ($ruteo["model"] == "comentario-by-id"){
		$result = $objComentario->obtenerComentarioPorId($conn,$id);
	}
?>