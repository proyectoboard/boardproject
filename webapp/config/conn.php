<?php 
ini_set('display_errors', 'Off');
error_reporting(0);

// CONSTANTES
define("API", "http://localhost/boardproject/");
define("BASEURL", "http://localhost/boardproject/webapp/");
// CONEXION
class Conn {


	public function _getQuery($model){
		$API = API;
		$result = file_get_contents($API . $model);
		return json_decode( $result );
	}

	public function _postQuery($params, $url){
		$postBody = $params;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, API . $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		
		// TOKEN POST		
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		// MAKE POST
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            json_encode($postBody) );

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
		curl_close ($ch);

		return json_decode( $server_output );
	}
}
?>