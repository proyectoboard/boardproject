<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="background-color: blue;">
  <a class="navbar-brand" href="dashboard.php">Virtual POST IT</a>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <?php if ($usuario->tipo=="administrador") { ?>
        <a class="nav-link" href="usuarios.php?action=all">Usuarios</a>
      <?php } ?>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="tareas.php?action=all">Tareas</a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="espera.php?action=all">Tareas en espera</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="comentario.php?action=all">Comentarios</a>
      </li> -->
     </ul>
  </div>
</nav>