<div class="container-fluid">
	<h1>TAREAS</h1>
</div>
<div class="container-fluid text-right">
	<?php if ($usuario->tipo=="administrador") { ?>
<a href="tareas.php?action=new" class="btn btn-primary float-right" >Crear tarea</a>
<?php } ?>
</div>
			
	<?php 
			$action = "";
			if(isset($_GET) && !empty($_GET["action"])){
				$action = $_GET["action"];
			}

			$success = "";
			if(isset($_GET) && !empty($_GET["success"])){
				$success = $_GET["success"];
			}
			if($success){ ?>
				<div class="alert alert-success" role="alert">
				  <?php echo $success; ?>
				</div>
			<?php } ?>

			<?php 
			$error = "";
			if(isset($_GET) && !empty($_GET["error"])){
				$error = $_GET["error"];
			}
			if($error){ ?>
				<div class="alert alert-danger" role="alert">
				  <?php echo $error; ?>
				</div>
			<?php } ?>
		
		</form>
<?php if($action == "all") { ?>
<div class="container-fluid board--component">
	<div class="row">
		<div class="col-3">
			<center><h3>TO-DO</h3></center>
			<div class="board--component__container">
				<?php 

				for ($i=0; $i < count($tareas) ; $i++) { 
					$tarea 	= $tareas[$i];
					
					if($tarea->estado_tarea=="toDo"){
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea->nombre; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea->descripcion; ?>
						    </p>
						    <a target="_new" href="tareas.php?action=edit&id=<?php echo $tarea->id; ?>" class="btn btn-secondary">
						    	VER TAREA

						    </a>
						  </div>
						</div>
					</div>
				<?php 
					}
				}
				 ?>
			</div>
		</div>
		<div class="col-3">
			<center><h3>IN-PROGRESS</h3></center>
			<div class="board--component__container">
				<?php for ($i=0; $i < count($tareas) ; $i++) { 
					$tarea 	= $tareas[$i];
					if($tarea->estado_tarea=="inProgress"){
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea->nombre; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea->descripcion; ?>
						    </p>
						    <a href="tareas.php?action=edit&id=<?php echo $tarea->id; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } 
					}
				?>
			</div>
		</div>
		<div class="col-3">
			<center><h3>IN-REVIEW</h3></center>
			<div class="board--component__container">
				<?php for ($i=0; $i < count($tareas) ; $i++) { 
					$tarea 	= $tareas[$i];
					if($tarea->estado_tarea=="inReview"){
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea->nombre; ?>
						  </div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea->descripcion; ?>
						    </p>
						    <a href="tareas.php?action=edit&id=<?php echo $tarea->id; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } 
			}
			?></div>
			
		</div>
		<div class="col-3">
			<center><h3>DONE</h3></center>
			<div class="board--component__container">
			<?php for ($i=0; $i < count($tareas) ; $i++) { 
					$tarea 	= $tareas[$i];
					if($tarea->estado_tarea=="done"){
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-success mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea->nombre; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea->descripcion; ?>
						    </p>
						    <a href="tareas.php?action=edit&id=<?php echo $tarea->id; ?>" class="btn btn-secondary"> 	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } 
			}
			?></div>
		</div>
	</div>
</div>
<?php } ?>
<?php if($action == "new" || $action == "edit") { ?>
	<div>

		<div class="col-sm-4" >
			
				<?php 
					$id = "";
					if(isset($_GET) && !empty($_GET["id"])){
						$id 	= $_GET["id"];
						$tar 	= $tareas;
					}
				?>

				<?php if ($usuario->tipo=="administrador") { ?>

					<?php if($action == "edit") { ?>
						<h4>Edicion de tarea y crear comentario</h4>
						<a href="comentarios.php?action=new" class="btn btn-primary float-right">Crear comentario</a>
						<?php if ($tar->hold==1) { ?>
							<a href="comentario.php?action=edit&id=<?php echo $tarea->id; ?>" class="btn btn-outline-danger float-right">Tarea en espera</a>
					<?php } ?>
					 <br>
				<?php }	?>

					<?php if($action == "new") { ?>
					<h4>Creacion de tarea</h4>

					<?php } ?>

				<?php } else { ?>

					<h4>Cambio estado de tarea y crear comentario</h4>

					<a href="comentarios.php?action=new" class="btn btn-primary float-right">Crear comentario</a>
					<a href="espera.php?action=new" class="btn btn-outline-danger float-right">Pasar tarea a espera</a>
					 <br>
					
				<?php } ?>

				<form id="newtarea" method="POST" action="controllers/post/tarea.controller.php">
					<br>
					<?php if ($action== "new") { ?>
						<input required type="text" name="nombre" class="form-control" placeholder="Nombre" value="<?php echo $tar->nombre ? $tar->nombre : '' ?>">
					<?php } else { ?> 
					<input type="text" name="nombre" class="form-control" placeholder="Nombre" value="<?php echo $tar->nombre ? $tar->nombre : '' ?>" readonly>
					<?php } ?>
					<?php if ($usuario->tipo=="administrador") { ?>
					<br>
					<input required type="text" value="<?php echo $tar->descripcion ? $tar->descripcion : '' ?>" name="descripcion" class="form-control" placeholder="Descripcion">
					<br>
					<h6>Seleccionar programador</h6>
					
					<select class="form-control" name="idUsuario" placeholder="Programador asignado">

						<?php for ($i=0; $i < count($programadores) ; $i++) { ?> 
						
						<option value=<?php echo $programador ->id ?> ><?php 

							$programador = $programadores [$i];
							echo $programador ->nombre ?></option>

						<?php } ?>
								 						 				
					</select>

					<h6>Fecha de entrega:</h6>
					<input type="date" name="fechafin" class="form-control" placeholder="fecha_fin" value="<?php echo $tar->fecha_fin ? $tar->fecha_fin : '' ?>">
					<br>

				<?php } else { ?>

					<br>
					<input required type="text" value="<?php echo $tar->descripcion ? $tar->descripcion : '' ?>" name="descripcion" class="form-control" placeholder="Descripcion" readonly>
					<br>

					<h6>Fecha de entrega:</h6>
					<input type="date" name="fechafin" class="form-control" placeholder="fecha_fin" value="<?php echo $tar->fecha_fin ? $tar->fecha_fin : '' ?>" readonly>
					<br>
					
					<?php } ?>

		<?php if($action == "edit") { 
			
			?>

				<br>
					<h6>Estado de la tarea</h6>

					<select name="estado_tarea" class="form-control">
				<option  value="toDo" <?php echo ($tar->estado_tarea == "toDo") ? 'selected' : '' ; ?> >toDo</option>
				<option value="inReview" <?php echo ($tar->estado_tarea == "inReview") ? 'selected' : '' ; ?> >inReview</option>
				<option value="inProgress" <?php echo ($tar->estado_tarea == "inProgress") ? 'selected' : '' ; ?> >inProgress</option>		
				<option value="done" <?php echo ($tar->estado_tarea == "done") ? 'selected' : '' ; ?> >done</option>	
			</select>
			<?php } ?>
 
					<br>
					
					<input type="hidden" name="idAdmin" value=<?php echo $usuario->id ?> >
					<!-- <input type="hidden" name="idUsuario" value=<?php echo $usuario->id ?> > -->
					
					<input type="hidden" name="id" value=<?php echo $tar->id ?> >
 
					

							
					<button class="btn btn-default" type="reset">Cancelar</button>
					<button class="btn btn-success" type="submit"><?php echo !empty($id) ? 'Editar' : 'Guardar'; ?></button>
					
					<!-- <button type="submit" class="btn btn-primary float-left">Guardar</button> -->

					</form>

					<br>

					
		</div>


	<div style="float width: 600px;" >

			<h4>Comentarios de la tarea</h4>

			<form id="comentario-by-id" method="GET" action="controllers/get/comentarios.controller.php">
				<input type="hidden" name="idTarea" value=<?php echo $tarea->id ?> >

			<div class="board--component__container">
			<?php 

			for ($i=0; $i < count($comentarios) ; $i++) { 
					$comentario 	= $comentarios[$i]; 
					 ?>

					<div class="card text-white bg-info mb-3" style="max-width: 40rem;">
					  <div class="card-header"><?php echo $comentario->fechaeditado;  ?></div>
					  <div class="card-body">
					    <h5 class="card-title">Comentario</h5>
					    <p class="card-text"><?php echo $comentario->descripcion; ?></p>
					  </div>
					</div>
					
					
				<?php } 
			
			?></div>
		</form>

		<br>
	</div>
<?php } ?>