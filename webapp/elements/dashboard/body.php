
<!DOCTYPE html>
<html>
<head>
	<title>BIENVENIDO A VIRTUAL POST IT</title>
</head>
<h1>Bienvenido a VIRTUAL POST IT.</h1>
<body background="postit.jpg">

</body>
</html>

<!-- <div class="container-fluid">
	<h1>BOARD</h1>
</div>
<div class="container-fluid text-right">
	<button class="btn btn-primary">Crear tarea</button>
</div>
<?php 
	// $tareas = array(
	// 				"toDo" => array(
	// 							array(
	// 								"titulo" 		=> "Titulo de la tarea",
	// 								"descripcion" 	=> "Descripcion de la tarea",
	// 								"id" 			=> 101
	// 							),
	// 							array(
	// 								"titulo" 		=> "Hacer API - Obtener proyecto por ID",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 102
	// 							),
	// 							array(
	// 								"titulo" 		=> "ASAP - Obtener proyectos",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 101
	// 							),
	// 							array(
	// 								"titulo" 		=> "ASAP - Obtener proyecto por ID",
	// 								"descripcion" 	=> "Some quick.",
	// 								"id" 			=> 102
	// 							)
	// 						),
	// 				"inProgress" => array(
	// 							array(
	// 								"titulo" 		=> "Revisar base de datos",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 103
	// 							),
	// 							array(
	// 								"titulo" 		=> "Deploy a ambiente test",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 104
	// 							),
	// 							array(
	// 								"titulo" 		=> "Revisar base de datos",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 1233
	// 							),
	// 							array(
	// 								"titulo" 		=> "Deploy a ambiente test",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 12322
	// 							)
	// 						),
	// 				"inReview" => array(
	// 							array(
	// 								"titulo" 		=> "Revisar base de datos",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 107
	// 							),
	// 							array(
	// 								"titulo" 		=> "Deploy a ambiente test",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 107
	// 							),
	// 							array(
	// 								"titulo" 		=> "Revisar base de datos",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 125
	// 							),
	// 							array(
	// 								"titulo" 		=> "Deploy a ambiente test",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 1236
	// 							)
	// 						),
	// 				"Done" => array(
	// 							array(
	// 								"titulo" 		=> "Revisar base de datos",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 107
	// 							),
	// 							array(
	// 								"titulo" 		=> "Deploy a ambiente test",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 107
	// 							),
	// 							array(
	// 								"titulo" 		=> "Revisar base de datos",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 125
	// 							),
	// 							array(
	// 								"titulo" 		=> "Deploy a ambiente test",
	// 								"descripcion" 	=> "Some quick example text to build on the card title and make up the",
	// 								"id" 			=> 1236
	// 							)
	// 						)
	// 					);

	//?>
<div class="container-fluid board--component">
	<div class="row">
		<div class="col-3">
			<h3>TO-DO</h3>
			<div class="board--component__container">
				<?php for ($i=0; $i < count($tareas) ; $i++) { 
					$tarea 	= $tareas[$i];

					if($tarea->estado_tarea=="toDo"){
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea["nombre"]; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea["descripcion"]; ?>
						    </p>
						    <a target="_new" href="tareas.php?action=edit&id=<?php echo $tarea["id"]; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-3">
			<h3>IN-PROGRESS</h3>
			<div class="board--component__container">
				<?php for ($i=0; $i < count($tareas["inProgress"]) ; $i++) { 
					$tarea 	= $tareas["inProgress"][$i];
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea["titulo"]; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea["descripcion"]; ?>
						    </p>
						    <a href="tareas.php?action=edit&id=<?php echo $tarea["id"]; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-3">
			<h3>IN-REVIEW</h3>
			<div class="board--component__container"><?php for ($i=0; $i < count($tareas["inReview"]) ; $i++) { 
					$tarea 	= $tareas["inReview"][$i];
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea["titulo"]; ?>
						  </div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea["descripcion"]; ?>
						    </p>
						    <a href="tareas.php?action=edit&id=<?php echo $tarea["id"]; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } ?></div>
			
		</div>
		<div class="col-3">
			<h3>DONE</h3>
			<div class="board--component__container">
			<?php for ($i=0; $i < count($tareas["inReview"]) ; $i++) { 
					$tarea 	= $tareas["inReview"][$i];
				?>
					<div class="board--component__cards">
						<div class="card text-white bg-success mb-3" style="max-width: 18rem;">
						  <div class="card-header"><?php echo $tarea["titulo"]; ?></div>
						  <div class="card-body">
						    <p class="card-text">
						    	<?php echo $tarea["descripcion"]; ?>
						    </p>
						    <a href="tareas.php?action=edit&id=<?php echo $tarea["id"]; ?>" class="btn btn-secondary">
						    	VER TAREA
						    </a>
						  </div>
						</div>
					</div>
				<?php } ?></div>
		</div>
	</div>
</div> -->