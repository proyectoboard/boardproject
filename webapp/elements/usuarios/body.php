<div class="container">
	<h2>Usuarios</h2>
	<?php 
		$action = "";
		if(isset($_GET) && !empty($_GET["action"])){
			$action = $_GET["action"];
		}
	?>

	<?php if($action == "all") { ?>
	<div class="">
		<a href="usuarios.php?action=new" class="btn btn-primary float-right">Crear usuario</a>
	</div>
	<table class="table table--software">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Telefono</th>
				<th>Direccion</th>
				<th>e-mail</th>
				<th>inicioAct</th>
				<th>Usuario</th>
				<th>Tipo</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php for ($i=0; $i < count($usuarios) ; $i++) { 
				$usr = $usuarios[$i];
			?>
				<tr>	
					<td><?php echo $usr->id; ?></td>
					<td><?php echo $usr->nombre; ?></td>
					<td><?php echo $usr->apellido; ?></td>
					<td><?php echo $usr->telefono; ?></td>
					<td><?php echo $usr->direccion; ?></td>
					<td><?php echo $usr->email; ?></td>
					<td><?php echo $usr->inicioAct; ?></td>
					<td><?php echo $usr->usuario; ?></td>
					<td><?php echo $usr->tipo; ?></td>
					<td class="text-right">
						<a class="btn btn-outline-primary" href="usuarios.php?action=edit&id=<?php echo $usr->id; ?>">
							Editar usuario
						</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?php } ?>

	<?php if($action == "new" || $action == "edit") { ?>
		<?php 
			$id = "";
			if(isset($_GET) && !empty($_GET["id"])){
				$id 	= $_GET["id"];
				$usr 	= $result;
			}
		?>
		<form id="login" method="POST" action="controllers/post/usuario.controller.php">
			<?php if(!empty($id) && $action == "edit") {?>
				<input name="id" type="number" readonly="" value="<?php echo $usr->id ? $usr->id : '' ?>" class="form-control">
				<br>
			<?php } ?>
			<input required type="text" name="nombre" class="form-control" placeholder="Nombre" value="<?php echo $usr->nombre ? $usr->nombre : '' ?>">
			<br>
			<input required type="text" value="<?php echo $usr->usuario ? $usr->usuario : '' ?>" name="usuario" class="form-control" placeholder="Usuario">
			<br>
			<input required type="text" name="apellido" class="form-control" placeholder="Apellido" value="<?php echo $usr->apellido ? $usr->apellido : '' ?>">
			<br>
			<input required type="text" name="telefono" class="form-control" placeholder="Telefono" value="<?php echo $usr->telefono ? $usr->telefono : '' ?>">
			<br>
			<input required type="text" name="direccion" class="form-control" placeholder="Direccion" value="<?php echo $usr->direccion ? $usr->direccion : '' ?>">
			<br>
			<input required type="text" name="email" class="form-control" placeholder="e-mail" value="<?php echo $usr->email ? $usr->email : '' ?>">
			<br>
			<select name="tipo" class="form-control">
				<option value="administrador" <?php echo ($usr->tipo == "administrador") ? 'selected' : '' ; ?> >ADMINISTRADOR</option>
				<option value="usuario" <?php echo ($usr->tipo == "usuario") ? 'selected' : '' ; ?> >USUARIO</option>
			</select>
			<br>
			<input type="password" name="clave" class="form-control" placeholder="Clave">
			<br>
			<?php 
			$success = "";
			if(isset($_GET) && !empty($_GET["success"])){
				$success = $_GET["success"];
			}
			if($success){ ?>
				<div class="alert alert-success" role="alert">
				  <?php echo $success; ?>
				</div>
			<?php } ?>

			<?php 
			$error = "";
			if(isset($_GET) && !empty($_GET["error"])){
				$error = $_GET["error"];
			}
			if($error){ ?>
				<div class="alert alert-danger" role="alert">
				  <?php echo $error; ?>
				</div>
			<?php } ?>
			<button class="btn btn-default" type="reset">Cancelar</button>
			<button class="btn btn-success" type="submit"><?php echo !empty($id) ? 'Editar' : 'Guardar'; ?></button>
		</form>
	<?php } ?>
</div>