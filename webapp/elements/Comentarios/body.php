<div class="container">
	<h2>Creacion de comentario</h2>
	<?php 
		$action = "";
		if(isset($_GET) && !empty($_GET["action"])){
			$action = $_GET["action"];
		}
	?>

	<?php if($action == "all") { ?>
	<div class="">
		<a href="comentario.php?action=new" class="btn btn-primary float-right">Crear comentario</a>
	</div>
	<table class="table table--software">
		<thead>
			<tr>
				<th>Descripcion</th>
				<th>Fecha Creado</th>
				<th>Usuario</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php for ($i=0; $i < count($comentarios) ; $i++) { 
				$com = $comentarios[$i];
			?>
				<tr>	
					<td><?php echo $com->descripcion; ?></td>
					<td><?php echo $com->fechacreado; ?></td>
					<td><?php echo $com->idUsuario; ?></td>
					<td class="text-right">
						<a class="btn btn-outline-primary" href="comentarios.php?action=edit&id=<?php echo $com->id; ?>">
							Editar comentario
						</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?php } ?>

	<?php if($action == "new" || $action == "edit") { ?>
		<?php 
			$id = "";
			if(isset($_GET) && !empty($_GET["id"])){
				$id 	= $_GET["id"];
				$com 	= $result;
			}
		?>
		<form id="login" method="POST" action="controllers/post/comentario.controller.php">
			<?php if(!empty($id) && $action == "edit") {?>
				<input name="id" type="number" hidden value="<?php echo $com->idTarea ? $com->idTarea : '' ?>" class="form-control">
				<br>
			<?php } ?>
			<input type="hidden" name="idTarea" value=<?php echo $tar->id ?> >
			<textarea rows="10" cols="50" required placeholder="Insertar comentario aqui" value="<?php echo $comentarios->descripcion ? $comentarios->descripcion : '' ?>"></textarea>
			<br>
			<?php 
			$success = "";
			if(isset($_GET) && !empty($_GET["success"])){
				$success = $_GET["success"];
			}
			if($success){ ?>
				<div class="alert alert-success" role="alert">
				  <?php echo $success; ?>
				</div>
			<?php } ?>

			<?php 
			$error = "";
			if(isset($_GET) && !empty($_GET["error"])){
				$error = $_GET["error"];
			}
			if($error){ ?>
				<div class="alert alert-danger" role="alert">
				  <?php echo $error; ?>
				</div>
			<?php } ?>
			<button class="btn btn-default" type="reset">Cancelar</button>
			<button class="btn btn-success" type="submit"><?php echo !empty($id) ? 'Editar' : 'Guardar'; ?></button>
		</form>
	<?php } ?>
