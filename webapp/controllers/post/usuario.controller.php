<?php 
	session_start();
	require('../../config/conn.php');
	$conn 	= new Conn();

	$url 	= "?model=usuario-new&action=insert";
	$acceso = $_POST;

	if(empty($acceso["id"])){
		$result = $conn->_postQuery($acceso,$url);

		if(!$result->error){
			header("location: ".BASEURL."usuarios.php?action=new&success=".$result->success);
		} else {
			header("location: ".BASEURL."usuarios.php?action=new&error=".$result->error);
		}
	} else {
		$url 	= "?model=usuario-edit&action=insert";
		$result = $conn->_postQuery($acceso,$url);

		if(!$result->error){
			header("location: ".BASEURL."usuarios.php?action=edit&id=".$acceso["id"]."&success=".$result->success);
		} else {
			header("location: ".BASEURL."usuarios.php?action=edit&id=".$acceso["id"]."&error=".$result->error);
		}
	}
	


?>