<?php 
	session_start();
	require('../../config/conn.php');
	$conn 	= new Conn();

	$url 	= "?model=tarea-new&action=insert";
	$acceso = $_POST;
	
	
	
	if(empty($acceso["id"])){
		$result = $conn->_postQuery($acceso,$url);


		if(!$result->error){
			header("location: ".BASEURL."tareas.php?action=new&success=".$result->success);
		} else {
			header("location: ".BASEURL."tareas.php?action=new&error=".$result->error);
		}
	} else {
		$url 	= "?model=tarea-edit&action=insert&id=".$acceso["id"];
		$result = $conn->_postQuery($acceso,$url);

		
		if(!$result->error){
			header("location: ".BASEURL."tareas.php?action=edit&id=".$acceso["id"]."&success=".$result->success);
		} else {
			header("location: ".BASEURL."tareas.php?action=edit&id=".$acceso["id"]."&error=".$result->error);
		}
	}
	
?>