-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 14-03-2019 a las 23:05:04
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `board_base`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE IF NOT EXISTS `archivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTarea` int(11) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTarea` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `fechacreado` datetime NOT NULL,
  `fechaeditado` datetime NOT NULL,
  `fechafin` datetime NOT NULL,
  `comentaHold` varchar(200) NOT NULL,
  `respuestaHold` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `idTarea`, `descripcion`, `activo`, `fechacreado`, `fechaeditado`, `fechafin`, `comentaHold`, `respuestaHold`) VALUES
(1, 1, 'Prueba de comentario en ID tarea', 1, '0000-00-00 00:00:00', '2019-03-08 00:00:00', '0000-00-00 00:00:00', '', ''),
(2, 7, '', 1, '0000-00-00 00:00:00', '2019-03-09 00:00:00', '0000-00-00 00:00:00', 'No entiendo el trabajo a realizar', ''),
(3, 8, 'Prueba', 1, '0000-00-00 00:00:00', '2019-03-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 9, 'Prueba', 1, '0000-00-00 00:00:00', '2019-03-01 00:00:00', '0000-00-00 00:00:00', 'Prueba de Hold en tarea', ''),
(5, 10, 'prueba de insertar comentario', 0, '2019-03-13 13:43:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 11, 'prueba de insertar comentario', 0, '2019-03-13 13:47:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 1, 'Comentario probado', 1, '2019-03-14 10:50:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(16, 0, '', 1, '2019-03-14 19:42:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE IF NOT EXISTS `tareas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_creado` date NOT NULL,
  `fecha_entrega` date NOT NULL,
  `idAdmin` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `fecha_fin` date NOT NULL,
  `fecha_editado` date NOT NULL,
  `estado_tarea` varchar(200) NOT NULL DEFAULT 'TO-DO',
  `hold` tinyint(4) NOT NULL DEFAULT '0',
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Volcado de datos para la tabla `tareas`
--

INSERT INTO `tareas` (`id`, `nombre`, `descripcion`, `fecha_creado`, `fecha_entrega`, `idAdmin`, `idUsuario`, `fecha_fin`, `fecha_editado`, `estado_tarea`, `hold`, `activo`) VALUES
(1, 'Prueba editada', 'Tarea editada con exito', '2019-02-28', '0000-00-00', 2, 5, '2019-02-28', '2019-03-13', 'inProgress', 0, 1),
(7, 'Prueba editada', 'Tarea editada con exito', '2019-03-26', '0000-00-00', 2, 2, '2019-02-27', '2019-03-14', 'inProgress', 1, 1),
(8, 'Prueba editada', 'Tarea editada con exito', '2019-03-14', '0000-00-00', 2, 1, '0000-00-00', '2019-03-14', 'inReview', 0, 1),
(9, 'Prueba editada', 'Tarea editada con exito', '2019-03-06', '0000-00-00', 2, 5, '0000-00-00', '2019-03-12', 'inReview', 1, 1),
(10, 'Prueba editada', 'Tarea editada con exito', '2019-03-07', '0000-00-00', 3, 2, '0000-00-00', '2019-03-14', 'toDo', 0, 1),
(11, 'Prueba editada', 'Tarea editada con exito', '2019-03-07', '0000-00-00', 3, 5, '0000-00-00', '2019-03-10', 'toDo', 0, 1),
(27, 'crear tarea en clase', 'prueba de creado', '2019-03-14', '0000-00-00', 2, 2, '0000-00-00', '0000-00-00', 'toDo', 0, 1),
(28, 'prueba', 'wqwqwqw', '2019-03-14', '0000-00-00', 2, 2, '0000-00-00', '0000-00-00', 'toDo', 0, 1),
(29, 'prueba', '12212212', '2019-03-14', '0000-00-00', 2, 2, '0000-00-00', '0000-00-00', 'toDo', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL DEFAULT 'usuario',
  `nombre` varchar(200) NOT NULL,
  `apellido` varchar(200) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `primeraV` tinyint(4) NOT NULL DEFAULT '1',
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `inicioAct` date NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `clave` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `tipo`, `nombre`, `apellido`, `telefono`, `direccion`, `email`, `primeraV`, `activo`, `inicioAct`, `usuario`, `clave`) VALUES
(1, 'usuario', 'Matias1223111', 'Olivieri123', '098123123', 'Montevideo', 'matiasolivieri34@gmail.com', 0, 1, '0000-00-00', 'matias', '202cb962ac59075b964b07152d234b70'),
(2, 'administrador', 'Dario', 'Carbajal', '099336444', 'Rep del Peru 815/712', 'intuihard@gmail.com', 0, 1, '2019-02-12', 'prueba', '202cb962ac59075b964b07152d234b70'),
(3, 'administrador', 'Jose', 'Perez', '090990909', 'su direccion', 'email@fkkjjd.com', 1, 1, '2019-02-21', 'jope', '202cb962ac59075b964b07152d234b70'),
(5, 'usuario', 'Andres1212121', 'Sanchez', '898989', 'lu direccion', 'email@fjd.com', 1, 1, '0000-00-00', 'Andy', '202cb962ac59075b964b07152d234b70'),
(13, 'usuario', 'Prueba usuario', 'carbajal', '0909090', 'direcion', 'intui', 1, 1, '2019-03-14', 'usuario', '202cb962ac59075b964b07152d234b70');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
