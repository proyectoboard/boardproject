<?php 
ini_set('display_errors', 'Off');
error_reporting(0);

// CONSTANTES
define("API", "http://localhost/boardproject/");

// CONEXION
class Conn {


	public function queryJSON($model){
		$API = API;
		$result = file_get_contents($API . "models/". $model . ".json");
		return $result;
	}

	private $conexion;

	public function __construct(){
		$servidor = "localhost";
		$usuario  = "root";
		$clave    = "";
		$base	  = "board_base";
		$this->conexion = mysqli_connect($servidor, $usuario, $clave, $base)
			or die('No se pudo conectar: ' . mysqli_error());
		mysqli_set_charset($this->conexion, 'utf8');
	}

	public function query($sql){
		$resultado = mysqli_query($this->conexion, $sql) or die('Consulta fallida: ' . mysqli_error($this->conexion));
		$datos = array();
		//guardamos en un array
		while($fila =  mysqli_fetch_array($resultado, MYSQLI_ASSOC)){
			$datos[] = $fila;
		}    	
		// Liberar resultados
		mysqli_free_result($resultado);

		return $datos;
	}
}
?>