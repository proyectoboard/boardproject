 <?php 
	// RUTEO
	$ruteo = array(
				"model"  => "",
				"action" => "",
				"params" => ""
			);

	$result = array("error" => "api not found");

	if(isset($_GET['model']) && isset($_GET['action'])){
		
		$ruteo["model"] 	= $_GET['model'];
		$ruteo["action"] 	= $_GET['action'];
		
		if($ruteo["action"] == "all"){

			if($ruteo["model"] == "tareas"){

				// LISTADO DE tareas
				require('components/tareas/index.php');

			} else if($ruteo["model"] == "usuarios"){

				// LISTADO DE USUARIOD
				require('components/usuarios/index.php');

			} else if ($ruteo["model"] == "obtenerUsuariosProgramador") {
				
				require('components/usuarios/get.php');

			} else if($ruteo["model"] == "comentarios"){

				// LISTADO DE Administradores
				require('components/comentarios/index.php');

			} else {
				$result = array("error" => $ruteo["model"] . " not found");
			}
			
		} else if($ruteo["action"] == "single"){

			if($ruteo["model"] == "tareas"|| $ruteo["model"] == "vertarea"|| $ruteo["model"] == "tareasforuser"){
				// LISTADO DE Tareas, Tareas, y por usuario
				require('components/tareas/get.php');

			} else if($ruteo["model"] == "usuarios" || $ruteo["model"] == "usuario-by-id"){
				// LISTADO DE Usuarios
				require('components/usuarios/get.php');

			} else if($ruteo["model"] == "comentario-by-id"){

				require('components/comentarios/get.php');

			} else {
				$result = array("error" => $ruteo["model"] . " not found");
			}
			
		} else if($ruteo["action"] == "insert"){
			if($ruteo["model"] == "tarea-new" || $ruteo["model"] == "tarea-edit"){

			
				require('components/tareas/post.php');
				
				} else if($ruteo["model"] == "login" || $ruteo["model"] == "usuario-new" || $ruteo["model"] == "usuario-edit"){
					
					require('components/usuarios/post.php');

			} else if($ruteo["model"] == "comentario-insert"){

				require('components/comentarios/post.php');
				}
			} else {

			$result = array("error" => true);
		}

	}
	
	
	header("Content-Type: application/json");
	echo json_encode( $result );
?>