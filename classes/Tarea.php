<?php 

// ini_set('display_errors', 'Off');
// error_reporting(0);

class Tarea {
	
	public function obtenerTareas($conn){
		
		$sql 	= "SELECT * 
				FROM tareas  WHERE activo=1";
		
		$result = $conn->query($sql);
		return $result;

	}

	public function obtenerTareasbyUsuario($conn, $id){
		//ver como traer el id del usuario que ingreso para filtrar las tareas de ese usuario
		$sql 	= "SELECT * 
				FROM tareas WHERE idUsuario='$id' ";
		
		$result = $conn->query($sql);
		return $result;

	}
	public function verTarea($conn, $id){
		
		$sql 	= "SELECT * 
				FROM tareas WHERE id='$id'";
		
		$result = $conn->query($sql);
		return $result[0];

	}
	public function obtenerTareashold($conn){
		//Consulta para traer las tareas en HOLD VER COMO FILTRAR ADMINs
		$sql 	= "SELECT * FROM tareas JOIN comentarios WHERE tareas.hold=1 AND tareas.id=comentarios.idTarea AND tareas.activo=1";

		$result = $conn->query($sql);
		return $result;

	}
	
	//Insertar Tarea a base
	public function insertarTarea($conn,$tarea, $id){
		$sql = "INSERT INTO tareas
				(nombre, descripcion, fecha_creado, fecha_entrega, idAdmin, idUsuario, fecha_fin, estado_tarea, hold, activo)
				VALUES
				('$tarea[nombre]','$tarea[descripcion]',NOW(),'$tarea[fecha_entrega]','$tarea[idAdmin]','$tarea[idUsuario]','$tarea[fecha_fin]','toDo',0 ,1 )
		";
		$result = $conn->query($sql);

		if(empty($result)){
			return array("success" => "Tarea creada correctamente.");
		} else {
			return array("error" => true, "sql" => $sql);
		}
	}
		public function editarTarea($conn, $tarea, $id){
		$sql = "UPDATE tareas SET
			nombre 			= '$tarea[nombre]',
			descripcion 	= '$tarea[descripcion]',
			idUsuario 		= '$tarea[idUsuario]',
			fecha_entrega 	= '$tarea[fecha_entrega]',
			estado_tarea	= '$tarea[estado_tarea]',
			fecha_editado	= NOW()
			WHERE id ='$id'";
		$result = $conn->query($sql);

		if(empty($result)){
			return array("success" => "Tarea editada correctamente.");
		} else {
			return array("error" => true, "sql" => $sql);
		}
	
	}
}

?>