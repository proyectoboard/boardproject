<?php
/**
* 
*/
class Usuario {
	
	public function login($conn,$usuario){
		// ENCRIPTAR CLAVE
		$usuario["clave"] = md5($usuario["clave"]);

		$sql = "SELECT * FROM usuarios
				WHERE usuario = '$usuario[usuario]'
				AND clave 	  = '$usuario[clave]'
		";
		$result = $conn->query($sql);

		if($result){
			return $result[0];
		} else {
			return array("error" => "usuario y/o clave incorrecta.");
		}
		
	}

	public function obtenerUsuarios($conn,$id) {

		// VERIFICAR SI ES ADMIN
		$sql = "SELECT * FROM usuarios
			WHERE id = '$id' AND tipo = 'administrador'
		";
		$result = $conn->query($sql);

		if($result){
			// OBTENER USUARIOS
			$sql 	= "SELECT id, nombre, apellido, telefono, direccion, inicioAct, email, usuario, tipo FROM usuarios WHERE tipo != 'administrador'";
			$result = $conn->query($sql);
			
			if($result){
				return $result;
			} else {
				return array("error" => "no existe usuarios");
			}
			
		} else {
			return array("error" => "usuario no tiene permisos");
		}
	}
public function ingresarUsuario($conn, $usuario){
		$usuario["clave"] = md5($usuario["clave"]);
		
		$sql 	= "INSERT INTO usuarios
					(tipo, nombre, apellido, telefono, direccion, email, inicioAct, usuario, clave)
				   VALUES
				   	('$usuario[tipo]','$usuario[nombre]','$usuario[apellido]','$usuario[telefono]','$usuario[direccion]','$usuario[email]', NOW(),'$usuario[usuario]','$usuario[clave]')
		";
		$result = $conn->query($sql);

		if(empty($result)){
			return array("success" => "Usuario creado correctamente.");
		} else {
			return array("error" => true, "sql" => $sql);
		}
	}

	public function obtenerUsuarioPorId($conn,$id){
		$sql = "SELECT id, nombre, apellido, direccion, email, telefono, usuario, tipo FROM usuarios WHERE id='$id' AND tipo <> 'administrador'";

		$result = $conn->query($sql);

		if($result){
			return $result[0];
		} else {
			return array("error" => "Usuario con ID: ". $id . " no encontrado.");
		}
	}

	public function obtenerUsuariosProgramador($conn){
		$sql = "SELECT id, nombre, apellido FROM usuarios WHERE tipo != 'administrador'";

		$result = $conn->query($sql);

		if($result){
			return $result;
		} else {
			return array("error" => "Usuario con ID: ". $id . " no encontrado.");
		}
	}

	public function editarUsuario($conn, $usuario){
		$sql = "UPDATE usuarios SET 
			nombre 		= '$usuario[nombre]',
			apellido 	= '$usuario[apellido]',
			telefono 	= '$usuario[telefono]',
			direccion 	= '$usuario[direccion]',
			email	 	= '$usuario[email]',
			usuario 	= '$usuario[usuario]',
			tipo 		= '$usuario[tipo]'

		";

		if(empty($usuario["clave"])){
			$sql .= "
				WHERE id = '$usuario[id]'
			";
		} else {
			$usuario["clave"] = md5($usuario["clave"]);
			$sql .= "
				,
				clave = '$usuario[clave]'
				WHERE id = '$usuario[id]'
			";
		}

		$result = $conn->query($sql);
		
		if(empty($result)){

			return array("success" => "Usuario editado correctamente.");
		} else {
			return array("error" => true, "sql" => $sql);
		}
	}

}

?>